# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* KOS Script Highlighting, tooltips, and Autocomplete for notepad ++
* 0.1
* Get Down with the Markdown! Here So I can look later [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* This has been tested and confirmed to work for Notepad++ v 7.2.2

* Installation (I don't know how to create a notepad++ plugin installer, I just know putting these files in place works!)
1. Install Notepad++
2. Copy userDefineLang.xml to C:\Users\<YOUR WINDOWS USERNAME>\AppData\Roaming\Notepad++
3. Copy KerboSCript.xml to <Notepadd++ Install DIR>\plugins\APIs\KerboSCript.xml
4. Start Notepad++ And begin editing!

### Contribution guidelines ###

1. Additional Functions go into Keywords3 in userDefineLang.xml   This adds them to the auto complete dialog.  The list is space
delimited.
2. Tooltips are provided for functions and parameters in KerboScript.xml  The basic syntax that is there is all I managed to get working,
you can mess around with it if you like.
3. Don't check in any color themes you create, I like the current one :D


### Licensing ###

DoWhatYouLikeLicense v 1.0

I'm not liable for what this XML does to your computer.  I don't care what you do with the files in this repo.  They're free as in 
freedom, free beer, free candy.  Duplicate, share, edit!  Just call me with any good improvements because I am working on doing this
in eclipse too, which is much harder, but i'd like to enjoy KOS Highlighting/autocomplete/etc. in the meantime!


### Contribution Code Of Ethics ###

Don't be a dick, be a dude.

### Who do I talk to? ###

* Jesse Redfield - jesse.redfield@gmail.com